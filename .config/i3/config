# i3 config file (v4)
#
###########################################################
# VARIABLES
###########################################################
set $dmenu \
    "i3-dmenu-desktop --dmenu=\\"dmenu -i \
    -p 'Run: ' \
    -fn 'Hack:bold:pixelsize=14'\\""

set $pmenu \
    "/usr/share/doc/pass/examples/dmenu/passmenu -i \
    -p 'Password: ' \
    -fn 'Hack:bold:pixelsize=14'"

set $lock \
    "i3lock --image=/home/chuck/.dotfiles/backgrounds/noisy_net.png --tiling"

###########################################################
# Colors
###########################################################
set $bg-color               #2f343f
set $inactive-bg-color      #262A33
set $text-color             #f3f4f5
set $inactive-text-color    #676e7d
set $urgent-bg-color        #e53935
set $indicator-color        #e2ac36
set $separator-color        #757575
set $xresource              #757575

# Window Colors
# Name                  Border              Background          Text                    Indicator
client.focused          $bg-color           $bg-color           $text-color	        $indicator-color
client.unfocused        $inactive-bg-color  $inactive-bg-color  $inactive-text-color    $indicator-color
client.focused_inactive $inactive-bg-color  $inactive-bg-color  $inactive-text-color    $indicator-color
client.urgent           $urgent-bg-color    $urgent-bg-color    $text-color             $indicator-color

###########################################################
# FONTS
###########################################################
# Font for window titles.
# Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 10

###########################################################
# KEYS
###########################################################
# mod key
set $mod Mod4

# Media Keys
bindsym XF86AudioMute \
    exec --no-startup-id "amixer set Master toggle" 

bindsym XF86AudioLowerVolume \
    exec --no-startup-id "amixer set Master 2%-"

bindsym XF86AudioRaiseVolume \
    exec --no-startup-id "amixer set Master 2%+"

bindsym XF86MonBrightnessDown \
    exec --no-startup-id lower_brightness

bindsym XF86MonBrightnessUp \
    exec --no-startup-id raise_brightness

bindsym XF86Display \
    exec --no-startup-id $lock

#bindsym XF86WLAN \
#   exec --no-startup-id 

bindsym XF86Tools \
    exec --no-startup-id "touchpadtoggle"

bindsym XF86Search \
    exec --no-startup-id "firefox"

bindsym XF86LaunchA \
    exec --no-startup-id $dmenu

bindsym XF86Explorer \
    exec --no-startup-id "thunar"

bindsym XF86AudioMicMute \
    exec --no-startup-id "amixer set Capture toggle"

# start a terminal
bindsym $mod+Return exec x-terminal-emulator

# kill focused window
bindsym $mod+Shift+q kill

# dmenu
bindsym $mod+d exec --no-startup-id $dmenu
bindsym $mod+Shift+x exec --no-startup-id dmenu_power

# passmenu
#bindsym $mod+p exec --no-startup-id $pmenu

# disable touchpad
bindsym $mod+t exec touchpadtoggle

# change focus Vim Style
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym j resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

###########################################################
# I3BLOCKS
###########################################################
bar {
	position bottom
        status_command i3blocks
	colors {
		background  $bg-color
		separator   $separator-color
		#			Border                  Background	        Text
		focused_workspace       $bg-color		$bg-color	        $text-color
		inactive_workspace	$inactive-bg-color	$inactive-bg-color	$inactive-text-color
		urgent_workspace	$urgent-bg-color        $urgent-bg-color        $text-color
	}
	 
}

# CAPS/NUM Lock Status
bindsym --release Caps_Lock exec pkill -SIGRTMIN+11 i3blocks
bindsym --release Num_Lock  exec pkill -SIGRTMIN+11 i3blocks

###########################################################
# FLOATING WINDOWS
###########################################################
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Set min and max floating size
floating_minimum_size 75x50
floating_maximum_size 1400x1000

# Various
for_window [window_role="Preferences"]  floating enable
for_window [window_role="bubble"]       floating enable
for_window [window_role="pop-up"]       floating enable
for_window [window_role="task_dialog"]  floating enable
for_window [window_type="dialog"]       floating enable
for_window [window_type="menu"]         floating enable
for_window [window_type="splash"]       floating enable
for_window [window_type="utility"]      floating enable

# Qalculate
for_window [class="Qalculate-gtk"] \
    floating enable, \
    resize set 800 600, \
    move position center, \
    border pixel 6
    
# Network Manager
for_window [class="Nm-connection-editor"] \
    floating enable, \
    resize set 900 600, \
    move position center, \
    border pixel 6


for_window [class="mpv"] \
    floating enable, \
    resize set 1280 720, \
    move position center, \
    border pixel 3

###########################################################
# SCRATCHPADS
###########################################################
# KeePassXC
bindsym $mod+p \
    [class="KeePassXC"] \
    scratchpad show, \
    move position center

for_window [class="KeePassXC"] \
    floating enable, \
    resize set 900 800, \
    border pixel 6, \
    move scratchpad

for_window [class="KeePassXC" window_type="dialog"] \
    floating enable, \
    scratchpad show, \
    move position center

for_window [class="KeePassXC" window_type="normal"] \
    floating enable, \
    scratchpad show, \
    move position center

# Python3 for basic math
bindsym $mod+m \
    [class="URxvt" instance="math"] \
    scratchpad show, \
    move position center

for_window [class="URxvt" instance="math"] \
    floating enable, \
    resize set 600 800, \
    border pixel 6, \
    move scratchpad

# Cmus
bindsym $mod+i \
    [class="URxvt" instance="music"] \
    scratchpad show, \
    move position center

for_window [class="URxvt" instance="music"] \
    floating enable, \
    resize set 900 600, \
    border pixel 6, \
    move scratchpad

# Signal
bindsym $mod+o \
    [class="Signal"] \
    scratchpad show, \
    move position center

for_window [class="Signal"] \
    floating enable, \
    resize set 600 900, \
    border pixel 6, \
    move scratchpad

###########################################################
# STARTUP
###########################################################
exec --no-startup-id "urxvtd -q -f -o"
#exec --no-startup-id "dispwin -d 1 -I /home/chuck/.dotfiles/icc/2020-04-08.icc"
#exec --no-startup-id "compton --daemon --shadow --fading"
exec --no-startup-id "feh --bg-tile ~/.dotfiles/backgrounds/noisy_net.png"
exec --no-startup-id "nm-applet"
exec --no-startup-id "keepassxc.appimage"
exec --no-startup-id "sh -c 'sleep 5'; exec nextcloud.appimage"
exec --no-startup-id "sh -c 'sleep 6'; urxvtc -name music -fn 'xft:Hack-Regular:size=14' -e cmus"
exec --no-startup-id "sh -c 'sleep 7'; urxvtc -name math -fn 'xft:Hack-Regular:size=18' -e python3 -q"
#exec --no-startup-id "udiskie --automount --smart-tray --notify"
